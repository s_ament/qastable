function [ pdf ] = stable_pdf( x, a, b )
% stable pdf computes the probability density function of
% a standard (mu = 0, sigma = 1) stable law in Zolotarev's M parameterization
% with stability parameter a and skew parameter b

if b == 0 % symmetric case
    
    pdf = stable_sym_pdf(x, a); 
    
else % asymmetric case
    
    z = - b * tan((pi*a)/2);
    
    pdf = zeros( size(x) );
    
    xlz_cond = x < z;
    if any( xlz_cond )
        
        pdf( xlz_cond ) = stable_pdf( - x( xlz_cond ), a, -b);
        
    end
    
    if any( ~xlz_cond )
        
        % number of terms to be used in the series expansions
        if 1.1 <= a

            n_inf = 80;

        elseif .9 < a || a < .5
            
            display('Error: .9 < alpha < 1.1 if beta != 0, and alpha < .5 not supported')
            pdf = NaN(size(x));
            return 
            
        elseif .5 <= a

            n_inf = 90;
            
        end
        
        % minimum x for which the series at infinity, truncated to n terms,
        % is accurate to machine epsilon
        min_inf_x = ( (1+z.^2).^(n_inf/2) ...
            .* a /(pi * eps) ...
            .* gamma(a*n_inf)./gamma(n_inf) ).^(1./(a*n_inf-1));

        min_inf_x = min_inf_x + z;
        
        inf_cond = min_inf_x < x;
        if any( inf_cond ) 
            pdf( inf_cond ) = stable_pdf_series_infinity( ...
                x( inf_cond ), a, b, n_inf);
        end

        fourier_cond = z < x & ~inf_cond;
        if any( fourier_cond )
           pdf( fourier_cond ) = stable_pdf_fourier_integral( ...
               x( fourier_cond ) , a, b); 
        end

    end

end

end

