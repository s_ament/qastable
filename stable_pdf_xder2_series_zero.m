function [ val ] = stable_pdf_xder2_series_zero( x, a, b, max_coef )

z = -b * tan((pi*a)/2);
     
k_index = linspace(0, max_coef, max_coef+1);

gamma_part = gamma( (k_index+3) / a) ./ factorial(k_index);

sqrt_1_plus_zeta = (1+z^2)^(-1/(2*a));
geometric_part = 1;

sin_part = sin( (pi/2 + atan(z) /a ) * (k_index + 1));

x_minus_z = (x-z);
x_part = 1./(x_minu_z);

val = zeros(size(x));

for k = 0:max_coef
    
   geometric_part = geometric_part * sqrt_1_plus_zeta;
   x_part = x_part .* x_minus_z;
   
   term = gamma_part(k+1) ...
         .* geometric_part ...
         .* sin_part(k+1) ...
         .* x_part;
     
   val = val + term;
end

val = val * 1/(a*pi);

end

