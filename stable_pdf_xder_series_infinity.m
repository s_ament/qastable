function [ val ] = stable_pdf_xder_series_infinity( x, a, b, num_terms )
%

z = -b * tan((pi*a)/2);

k_index = linspace(1, num_terms, num_terms);

sign = 1;
  
gamma_part = gamma( a * k_index ) ./ factorial(k_index-1);

sqrt_1_plus_zeta = sqrt(1+z^2);
geometric_part = 1;

sin_part = sin( (pi/2 * a - atan(z)) * k_index);
x_to_minus_a = (x-z).^(-a);
x_part = 1 ./ (x-z).^2;
 
val = zeros(size(x));

for k = 1:num_terms
    
   sign = -sign;
   
   geometric_part = geometric_part * sqrt_1_plus_zeta;
   x_part = x_part .* x_to_minus_a;
   
   term = sign...
         * (a*k+1) ...
         .* gamma_part(k) ...
         .* geometric_part ...
         .* sin_part(k)...
         .* x_part;
     
   val = val + term;
   
end

val = val * a/pi;

end

