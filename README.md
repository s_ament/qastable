# Summary

`qastable` provides functions for the numerical computation of stable densities,
distribution functions, and partial derivatives of the densities.
The code relies on optimized generlized Gaussian quadrature rules, and 
asymptotic expansions.
This makes it possible to compute stable distributions very efficiently 
up to virtually machine precision.
Indeed, `qastable` outperforms most adaptive integrations schemes for 
stable distributions by several orders of magnitude.
The file `stable_demo.m` shows how to use the core functions of `qastable`.

For more information on the methodology, please see our [article here](https://arxiv.org/abs/1607.04247).

# Citing 

If you use `qastable` for a publication, please cite the following article:

```
@article{ament2018accurate,
  title={Accurate and efficient numerical calculation of stable densities via optimized quadrature and asymptotics},
  author={Ament, Sebastian and O’Neil, Michael},
  journal={Statistics and Computing},
  volume={28},
  pages={171--185},
  year={2018},
  publisher={Springer}
}
```
