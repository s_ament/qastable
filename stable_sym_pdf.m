function [ pdf ] = stable_sym_pdf( x, a )%, mu, sigma)
% computes symmetric stable density values

%x = (x - mu) / sigma;

if a == 2
    
    pdf = 1/(sqrt(2*pi)) * exp(-x.^2/2);  % Gaussian
    
elseif a == 1
    
    pdf = 1./ ( pi * ( 1 + x.^2) ); % Cauchy
    
elseif .5 <= a && a < 2 
   
    % number of terms to be used in the series expansion
    n_inf = 42;

    % minimum x for which the series at infinity, truncated to n terms,
    % is accurate to machine precision.
    min_inf_x = (a /(pi * eps) ...
            .* gamma(a*n_inf)./gamma(n_inf) ).^(1./(a*n_inf-1));
    
    pdf = zeros(size(x));
    
    neg_inf_cond = x < -min_inf_x;
    if any(neg_inf_cond)
        x( neg_inf_cond ) = - x( neg_inf_cond );
    end
    
    inf_cond = min_inf_x < x;
    if any( inf_cond )
        pdf( inf_cond ) = stable_pdf_series_infinity(...
            x(inf_cond), a, 0, n_inf);
    end
    
    fourier_cond = ~inf_cond;
    if any( fourier_cond )
        pdf( fourier_cond ) = stable_sym_pdf_fourier_integral( ...
            x( fourier_cond ) , a);
    end
    
else
    
    display('Error: Input a has to be between .5 and 2');
    pdf = Nan(size(x));

end

% pdf = pdf / sigma;

end

