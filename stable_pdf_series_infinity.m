function [ val ] = stable_pdf_series_infinity( x, alpha, beta, max_coef )

zeta = -beta * tan((pi*alpha)/2);

k_index = linspace(0, max_coef, max_coef+1);

sign = -1;
  
gamma_part = gamma( alpha * (k_index+1) ) ./ factorial(k_index);

sqrt_1_plus_zeta = sqrt(1+zeta^2);
geometric_part = 1;

sin_part = sin( (pi/2 * alpha - atan(zeta)) * (k_index + 1));
x_to_minus_a = (x-zeta).^(-alpha);
x_part = 1 ./ (x-zeta);
 
val = zeros(size(x));

for k = 0:max_coef
    
   sign = -sign;
   
   geometric_part = geometric_part * sqrt_1_plus_zeta;
   x_part = x_part .* x_to_minus_a;
   
   term = sign...
         .* gamma_part(k+1) ...
         .* geometric_part ...
         .* sin_part(k+1)...
         .* x_part;
     
   val = val + term;
   
end

val = val * alpha/pi;

end

