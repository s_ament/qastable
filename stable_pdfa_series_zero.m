function [ val ] = stable_pdfa_series_zero( x, a, b, max_coef )
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here
z = -b * tan((pi*a)/2);
z_d = -(pi*b*(tan((pi*a)/2)^2 + 1))/2;
%{
term = @(k, x, a, b) gamma( k./a + 1) ../ factorial(k) ...
         ..* (1 + zeta.^2)..^(- k ./ (2.*a)) ...
         ..* cos( ( pi./2 + atan(zeta) ./ a) .* k)...
         ..* (x - zeta)..^k;
%}

%{
term = @(k,x,a,b) (sin((pi/2 + atan(z)./a).*(k + 1)).*gamma((k + 1)./a).*(x - z).^k.*((log(z.^2 + 1).*(k + 1))./(2.*a.^2.*(z.^2 + 1).^((k + 1)./(2.*a))) - (z.*z_d.*(k + 1))./(a.*(z.^2 + 1).^((k + 1)./(2.*a) + 1))))./factorial(k) - (k.*sin((pi./2 + atan(z)./a).*(k + 1)).*gamma((k + 1)./a).*(x - z).^(k - 1).*z_d)./(factorial(k).*(z.^2 + 1).^((k + 1)./(2.*a))) - (gamma((k + 1)./a).*cos((pi./2 + atan(z)./a).*(k + 1)).*(x - z).^k.*(atan(z)./a.^2 - z_d./(a.*(z.^2 + 1))).*(k + 1))./(factorial(k).*(z.^2 + 1).^((k + 1)./(2.*a))) - (sin((pi./2 + atan(z)./a).*(k + 1)).*gamma((k + 1)./a).*psi((k + 1)./a).*(x - z).^k.*(k + 1))./(a.^2.*factorial(k).*(z.^2 + 1).^((k + 1)./(2.*a)));
%}

term = @(k,x,a,b) - (x.^k.*gamma((k + 1)/a).*sin((pi*(k + 1))/2))./(pi*a^2*factorial(k)) - (x.^k.*gamma((k + 1)/a).*psi((k + 1)/a).*sin((pi*(k + 1))/2).*(k + 1))./(pi*a^3*factorial(k));

K = linspace(0,max_coef,max_coef+1);
val = zeros(1,length(x));

for i = 1:length(x)
   val(i) = sum(term(K, x(i), a, b));
end

plot(term(K,x(1), a, b))

end

