function [ I ] = stable_pdf_integrand( theta, x, alpha, beta )
% stable_density_integrand computes the integrand of the 
% stationary phase integral (see Nolan)

t = theta;
a = alpha;
b = beta;

if a ~= 1 
    
    z =  -b * tan( (pi*a)/2 );

    if x == z
        display( ' x equal to zeta ' );
        I = zeros(size(x));
    else
        
        % update input according to symmetry
        if x < z
            x = - x;
            b = - b;
            z =  -b * tan( (pi*a)/2 );
        end

        t_0 = 1/a * atan( - z );

        cos_t = cos(t);
        exp_1 = 1/(a-1);
        exp_2 = a * exp_1;

        V = cos( a * t_0 )^(exp_1) .* (cos_t ./ sin(a * (t + t_0) )).^(exp_2)...
        .* cos( a * t_0 + (a-1) * t ) ./ cos_t;

        g = (x - z ).^(exp_2) .* V;
        I = g .* exp( -g );
            
    end
    
else
    
    z = 0;
    t_0 = pi / 2;
    V = (2 / pi) * ( (pi / 2) + b * t ) ./ cos( t ) .* exp( (1 / b)...
        * ( pi / 2 + b * t) .* tan( t ) );
    
    g_a1 = exp( - (pi / 2) * (x / b) ) .* V;
    I = g_a1 .* exp( - g_a1 );
    
end

% set all entries to zero that are not in the domain of the integrand
I( t <= -t_0 + eps | t >= pi/2 - eps ) = 0;
I( isnan(I) ) = 0;
end
